import React, { Component } from "react";

class Counter extends Component {
  /*
  state = {
    value: this.props.counter.value
  };

  handleIncrement = () => {
    this.setState({ value: this.state.value + 1 }); //this.state.value++; Reactでは反応しない。setStateを使う。
  };
  */

  componentDidUpdate(prevProps, prevState) {
    console.log("prevProps", prevProps);
    console.log("prevState", prevState);
    if (prevProps.counter.value !== this.props.counter.value) {
      //After that, maybe let Ajax call and get new data from the server...
    }
  }

  componentWillUnmount() {
    console.log("Counter - Unmount");
  }

  render() {
    console.log("Counter - Rendered");

    return (
      <div>
        <span className={this.getBadgeClasses()}>{this.formatCount()}</span>
        <button
          onClick={() => this.props.onIncrement(this.props.counter)}
          className="btn btn-secondary btn-sm"
        >
          Increment
        </button>
        <button
          onClick={() => this.props.onDelete(this.props.counter.id)}
          className="btn btn-danger btn-sm m-2"
        >
          Delete
        </button>
      </div>
    );
  }

  getBadgeClasses() {
    let classes = "badge m-2 badge-";
    classes += this.props.counter.value === 0 ? "warning" : "primary";
    return classes;
  }

  formatCount() {
    const { value } = this.props.counter;
    return value === 0 ? "Zero" : value;
  }
}

export default Counter;

/*
import React, { Component } from "react"; でReact.createElement(変数 （1つだけ！！ 要素が1つ以上の場合はDIVかReact.Fragmentを使う。）,場所 )をわざわざ書かなくても良い.

JS ではreturnの後に何も書かずに次の行に行ってしまうと、return;ということになるため、returnの後に(引数)をつける。

React.Fragment ==> 検証モードではDivを出さない。よりシンプルになる。

state = はReactのスペシャルプロパティ。 Object include any data this components need.　引き渡すデータ。Def みたいなイメージ

const {count} = this.state; ==> === this.state.count をリファクタリング。　もとは return this.state.count === 0 ? 'Zero' : this.state.count;      (if xxx then yyy otherwise zzz)

JSX では class=" " ではなく、className=" "とする。

let classes はBootstrapでのスタイルの指定でCountの数値によってスタイルを変えるようにしている。letは変数を変化できる。constは上書きできない。

<li key={tag}> ==> liの中にKeyをいれないと、検証のConsoleでWarningが出る。「Warning:Each child in an array or iterator should have a unique "key" prop.」

ReactではFor分を使わないよう。なので代わりにMapを使用している.

{this.state.tags.length === 0 && "Please create a new tag!"} ==> JSでは文字列だとから("") 数字だと0がFalse,それ以外だとTrueとして最後の関数が返り値となる。↑の場合だと、返り値は"Please create a new tag!"になる。
onClick={this.handleIncrement} ==>this.handleIncrementに()はつかないから注意。this.handleIncrementはCallしていない。onClickはEventなので。。。

constructor(xxx)はいわゆるコンストラクタです。ES6のクラスベースでReactコンポーネントを作るときはこのメソッドでコンポーネントを初期化します。明示的にコンストラクタを定義しない場合はスーパークラスのコンストラクタが呼ばれる。classにconstructor()メソッドを追加して、this.stateにオブジェクトとして初期Stateの値を定義します。でないと、コンポーネントはReact Elementと呼ばれるもので構築され、継承されているので、オーバーライドしないと意図した設計(this)で使用できない事がある。ルールは「constructor(){super()}」.Thisを使う場合はさらにAutobindingする。↓
ReactComponentでthisがundefinedになる問題の解決策 ==> http://c16e.com/1507060541/
https://segmentfault.com/a/1190000008165717
bind methodはvalue of this をセットできる。（初期化）　じゃないと、定義したThisはどこのThisを使えばよいのかわからなくなり、undifinedのエラーが返る。
もしくは、アロー関数になおすとConstructorはいらない。 ==> https://qiita.com/cubdesign/items/ee8bff7073ebe1979936

increment  変数の値を1増やす

* Arrow関数は引数が一つだとカッコを省略できる。　https://qiita.com/may88seiji/items/4a49c7c78b55d75d693b
*/

/* Thisの定義と初期化
  constructor() {
    super();
    this.handleIncrement = this.handleIncrement.bind(this);
  }

  handleIncrement() {
    console.log("increment clicked", this); //ここのthisは↑のconstructorで初期化しないとundifinedのエラーが返る。
  }
でも、毎回constructorを設定しては面倒なので、アロー関数でhandleIncrementをコードすれば解決できる↓

  handleIncrement = () => {
    console.log("increment Clicked", this);
  };
  これだけ。Constructorは要らない。

setState() methodはReactにstateをアップデートするよう伝える。継承されたreactにあるMethod. DOMとVirsual DOMのSync
使い方はsetState({xxx: code}) で、()の中に{}があることで、state= {}のオブジェクの{}を選択して、stateをアップデートする。
*/
