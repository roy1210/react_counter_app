import React, { Component } from "react";
import Counter from "./counter";

class Counters extends Component {
  render() {
    console.log("Counters - Rendered");

    const { onReset, counters, onDelete, onIncrement } = this.props;
    //下の{}のなかのプロパティにthis.propsを入れるのを省ける。つまり、{this.props.onReset}と同じ。

    return (
      <div>
        <button onClick={onReset} className="btn btn-primary btn-sm m-2">
          Reset
        </button>
        {counters.map(counter => (
          <Counter
            key={counter.id}
            onDelete={onDelete}
            onIncrement={onIncrement}
            counter={counter}
            //value={counter.value}
            //id={counter.id}
          />
        ))}
      </div>
    );
  }
}

export default Counters;

/*
 *<Counter key={counter.id} value={counter.value}/> ==> value={counter.value}の後ろには selected={true} が隠れている(省略。将来文法が変わるかもしれない。)これとconter.jsxのStateの value: this.props.valueで最初に表示されるValueは設定した通りになる。この場合、ID1のValueは4。

*props:Every react components has proparty called props
statusとの違い。Propは参照するだけ。Read Only。 X this.prop.value = 0

*const counters = [...this.state.counters]; ==> ...はクローンの意味。つまり、まず初期化されているstateと全く同じ配列をコピーして、それを counters 変数と名付ける。
*/
