import React, { Component } from "react";

//Stateless Functionla components　状態を持たないコンポーネント

const NavBar = ({ totalCounters }) => {
  console.log("NavBar - Rendered");

  return (
    <nav className="navbar navbar-light bg-light">
      <a className="navbar-brand" href="#">
        Navbar{" "}
        <span className="badge-pill badge-secondary">{totalCounters}</span>
      </a>
    </nav>
  );
};

export default NavBar;

/*

*{this.props.totalCounters} ==> thisはClassだけに使える。今回の場合Stateless Functionla componentsでClassじゃないから、アロー関数に引数をpropsをいれて、{this.props.totalCounters} にする。

*アロー関数の引数にオブジェクトを入れると、propsを入れる必要がなくなる。const NavBar = ({ totalCounters }) => { ====>  {totalCounters}

*Lifecycle hookはClassのみに使える。SFCの場合は

*/
